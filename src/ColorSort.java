
public class ColorSort 
{

enum Color {red, green, blue};

public static void main (String[] param) 
		{
   		// for debugging
		}

public static void reorder (Color[] balls) 
	{
	int r = 0;
	int g = 0;
	int b = 0;
	
	
	
	// to count each colored ball
	for (int i = 0; i< balls.length; i++)
		{
		if (balls[i] == Color.red)
			{
			r++;
			}
		else if (balls[i] == Color.green)
			{
			g++;
			}
		else if (balls[i] == Color.blue)
			{
			b++;
			}
		}
	
	
    // check if balls are of same color
    if (r == balls.length) 
    	{
        return;
    	} 
    else if (g == balls.length) 
    	{
        return;
    	}
    else if (g == balls.length) 
    	{
    	return;
    	}
	
	
	
	
	// to set new content of array, according to RGB occurrence
	for (int i = 0; i < r; i++)
		{
		balls[i] = Color.red;
		}
	
	for (int i = r; i < (balls.length - b); i++)
		{
		balls[i] = Color.green;
		}
	
	for (int i = (r+g); i < balls.length; i++)
		{
		balls[i] = Color.blue;
		}
	}
}

/* References:
1) https://git.wut.ee/i231/home1/src/master/src/Balls.java
2) http://stackoverflow.com/questions/11912520/reorder-array-of-red-blue-and-green-balls
*/